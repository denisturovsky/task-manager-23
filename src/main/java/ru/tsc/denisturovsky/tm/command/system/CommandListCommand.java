package ru.tsc.denisturovsky.tm.command.system;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.denisturovsky.tm.api.model.ICommand;
import ru.tsc.denisturovsky.tm.command.AbstractCommand;

import java.util.Collection;
import java.util.Locale;

public final class CommandListCommand extends AbstractSystemCommand {

    @NotNull
    public static final String ARGUMENT = "-cmd";

    @NotNull
    public static final String NAME = "commands";

    @NotNull
    public static final String DESCRIPTION = "Show command list";

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getArgument() {
        return ARGUMENT;
    }

    @Override
    public void execute() {
        System.out.format("[%s] \n", NAME.toUpperCase(Locale.ROOT));
        @NotNull final Collection<AbstractCommand> commands = getCommandService().getTerminalCommands();
        commands.forEach(m -> {
            @NotNull final String name = m.getName();
            if (!name.isEmpty()) System.out.println(name);
        });
    }

}

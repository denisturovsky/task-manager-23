package ru.tsc.denisturovsky.tm.command.system;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.denisturovsky.tm.api.service.ICommandService;
import ru.tsc.denisturovsky.tm.command.AbstractCommand;
import ru.tsc.denisturovsky.tm.enumerated.Role;

public abstract class AbstractSystemCommand extends AbstractCommand {

    @NotNull
    protected ICommandService getCommandService() {
        assert serviceLocator != null;
        return serviceLocator.getCommandService();
    }

    @Nullable
    public Role[] getRoles() {
        return null;
    }

}

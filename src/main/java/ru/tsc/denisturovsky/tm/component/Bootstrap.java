package ru.tsc.denisturovsky.tm.component;

import lombok.Getter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.denisturovsky.tm.api.repository.ICommandRepository;
import ru.tsc.denisturovsky.tm.api.repository.IProjectRepository;
import ru.tsc.denisturovsky.tm.api.repository.ITaskRepository;
import ru.tsc.denisturovsky.tm.api.repository.IUserRepository;
import ru.tsc.denisturovsky.tm.api.service.*;
import ru.tsc.denisturovsky.tm.command.AbstractCommand;
import ru.tsc.denisturovsky.tm.command.project.*;
import ru.tsc.denisturovsky.tm.command.system.*;
import ru.tsc.denisturovsky.tm.command.task.*;
import ru.tsc.denisturovsky.tm.command.user.*;
import ru.tsc.denisturovsky.tm.enumerated.Role;
import ru.tsc.denisturovsky.tm.enumerated.Status;
import ru.tsc.denisturovsky.tm.exception.system.ArgumentNotSupportedException;
import ru.tsc.denisturovsky.tm.exception.system.CommandNotSupportedException;
import ru.tsc.denisturovsky.tm.model.Project;
import ru.tsc.denisturovsky.tm.model.Task;
import ru.tsc.denisturovsky.tm.model.User;
import ru.tsc.denisturovsky.tm.repository.CommandRepository;
import ru.tsc.denisturovsky.tm.repository.ProjectRepository;
import ru.tsc.denisturovsky.tm.repository.TaskRepository;
import ru.tsc.denisturovsky.tm.repository.UserRepository;
import ru.tsc.denisturovsky.tm.service.*;
import ru.tsc.denisturovsky.tm.util.DateUtil;
import ru.tsc.denisturovsky.tm.util.TerminalUtil;

public final class Bootstrap implements IServiceLocator {

    @NotNull
    private final ICommandRepository commandRepository = new CommandRepository();

    @Getter
    @NotNull
    private final ICommandService commandService = new CommandService(commandRepository);

    @Getter
    @NotNull
    private final ILoggerService loggerService = new LoggerService();

    @NotNull
    private final IProjectRepository projectRepository = new ProjectRepository();

    @Getter
    @NotNull
    private final IProjectService projectService = new ProjectService(projectRepository);

    @NotNull
    private final ITaskRepository taskRepository = new TaskRepository();

    @Getter
    @NotNull
    private final ITaskService taskService = new TaskService(taskRepository);

    @Getter
    @NotNull
    private final IProjectTaskService projectTaskService = new ProjectTaskService(projectRepository, taskRepository);

    @NotNull
    private final IUserRepository userRepository = new UserRepository();

    @Getter
    @NotNull
    private final IUserService userService = new UserService(userRepository, projectRepository, taskRepository);

    @Getter
    @NotNull
    private final IAuthService authService = new AuthService(userService);

    {
        registry(new ApplicationAboutCommand());
        registry(new ApplicationExitCommand());
        registry(new ApplicationHelpCommand());
        registry(new ApplicationInfoCommand());
        registry(new ApplicationVersionCommand());
        registry(new ArgumentListCommand());
        registry(new CommandListCommand());

        registry(new ProjectChangeStatusByIdCommand());
        registry(new ProjectChangeStatusByIndexCommand());
        registry(new ProjectClearCommand());
        registry(new ProjectCompleteByIdCommand());
        registry(new ProjectCompleteByIndexCommand());
        registry(new ProjectCreateCommand());
        registry(new ProjectListCommand());
        registry(new ProjectRemoveByIdCommand());
        registry(new ProjectRemoveByIndexCommand());
        registry(new ProjectShowByIdCommand());
        registry(new ProjectShowByIndexCommand());
        registry(new ProjectStartByIdCommand());
        registry(new ProjectStartByIndexCommand());
        registry(new ProjectUpdateByIdCommand());
        registry(new ProjectUpdateByIndexCommand());

        registry(new TaskBindToProjectCommand());
        registry(new TaskChangeStatusByIdCommand());
        registry(new TaskChangeStatusByIndexCommand());
        registry(new TaskClearCommand());
        registry(new TaskCompleteByIdCommand());
        registry(new TaskCompleteByIndexCommand());
        registry(new TaskCreateCommand());
        registry(new TaskListCommand());
        registry(new TaskRemoveByIdCommand());
        registry(new TaskRemoveByIndexCommand());
        registry(new TaskShowByIdCommand());
        registry(new TaskShowByIndexCommand());
        registry(new TaskShowByProjectIdCommand());
        registry(new TaskStartByIdCommand());
        registry(new TaskStartByIndexCommand());
        registry(new TaskUnbindFromProjectCommand());
        registry(new TaskUpdateByIdCommand());
        registry(new TaskUpdateByIndexCommand());

        registry(new UserChangePasswordCommand());
        registry(new UserLockCommand());
        registry(new UserLoginCommand());
        registry(new UserLogoutCommand());
        registry(new UserRegistryCommand());
        registry(new UserRemoveCommand());
        registry(new UserShowProfileCommand());
        registry(new UserUnlockCommand());
        registry(new UserUpdateCommand());
    }

    private void initData() {
        @NotNull final User user = userService.create("user", "user", "user@user.ru");
        @NotNull final User admin = userService.create("admin", "admin", Role.ADMIN);

        taskService.add(user.getId(), new Task("TASK TEST", Status.IN_PROGRESS, DateUtil.toDate("10.05.2018")));
        taskService.add(user.getId(), new Task("TASK BETA", Status.NOT_STARTED, DateUtil.toDate("05.11.2020")));
        taskService.add(user.getId(), new Task("TASK BEST", Status.IN_PROGRESS, DateUtil.toDate("15.10.2019")));
        taskService.add(user.getId(), new Task("TASK MEGA", Status.COMPLETED, DateUtil.toDate("07.03.2021")));

        projectService.add(user.getId(), new Project("PROJECT DEMO", Status.IN_PROGRESS, DateUtil.toDate("07.05.2018")));
        projectService.add(user.getId(), new Project("PROJECT TEST", Status.NOT_STARTED, DateUtil.toDate("03.11.2020")));
        projectService.add(user.getId(), new Project("PROJECT BEST", Status.IN_PROGRESS, DateUtil.toDate("05.10.2019")));
        projectService.add(user.getId(), new Project("PROJECT MEGA", Status.COMPLETED, DateUtil.toDate("04.03.2021")));
    }

    private void initLogger() {
        loggerService.info("** WELCOME TO TASK MANAGER **");
        Runtime.getRuntime().addShutdownHook(
                new Thread(() -> loggerService.info("** TASK MANAGER IS SHUTTING DOWN **"))
        );
    }

    private void registry(@NotNull final AbstractCommand command) {
        command.setServiceLocator(this);
        commandService.add(command);
    }

    public void run(@Nullable final String[] args) {
        if (processArgument(args)) new ApplicationExitCommand().execute();
        initLogger();
        initData();
        while (true) {
            try {
                System.out.println("Enter command:");
                @NotNull final String command = TerminalUtil.nextLine();
                processCommand(command);
                loggerService.command(command);
                System.out.println("[OK]");
            } catch (@NotNull final Exception e) {
                loggerService.error(e);
                System.err.println("[FAIL]");
            }
        }
    }

    public boolean processArgument(@Nullable final String[] args) {
        if (args == null || args.length == 0) return false;
        @Nullable final String arg = args[0];
        processArgument(arg);
        return true;
    }

    public void processArgument(@Nullable final String argument) {
        @Nullable final AbstractCommand abstractCommand = commandService.getCommandByArgument(argument);
        if (abstractCommand == null) throw new ArgumentNotSupportedException(argument);
        authService.checkRoles(abstractCommand.getRoles());
        abstractCommand.execute();
    }

    public void processCommand(@Nullable final String command) {
        @Nullable final AbstractCommand abstractCommand = commandService.getCommandByName(command);
        if (abstractCommand == null) throw new CommandNotSupportedException(command);
        authService.checkRoles(abstractCommand.getRoles());
        abstractCommand.execute();
    }

}
